/* 
	Simple plugin for CPP
*/

#include "SDK/amx/amx.h"
#include "SDK/plugincommon.h"

#include "Main.h"
#include "Natives.h"

logprintf_t logprintf;

extern void
	*pAMXFunctions
;

PLUGIN_EXPORT bool PLUGIN_CALL Load(void **ppData) {
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];

	logprintf("\n LSP Server Plugin v%s \n", VERSION);
	return 1;
}

PLUGIN_EXPORT bool PLUGIN_CALL Unload() {
	return 1;
}

PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() {
	return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES;
}

AMX_NATIVE_INFO amx_Natives[] = {
	{"LSP_GetServerConfig", Natives::GetServerConfig}
};

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX *pAMX) {
	return amx_Register(pAMX, amx_Natives, -1);
}

PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX *pAMX) {
	return AMX_ERR_NONE;
}