#define _CRT_SECURE_NO_WARNINGS

#include "SDK/amx/amx.h"
#include "SDK/plugincommon.h"

#include <fstream>
#include "Main.h"
#include "LSP.h"

FILE *server_cfg = NULL;

LSP::LSP(void)
{
	
}


LSP::~LSP(void)
{
	
}


void LSP::servercfgvalue(char *opt, char **ret, int size)
{
	FILE *file = fopen("lsp.cfg", "r");
	char *value = new char[size];
	char *Buf = new char[size];
	while(!feof(file))
	{	
		fgets(Buf, size, file);
		strcpy(value, Buf);
		strtok(value, " ");
		if(!strcmp(value, opt))
		{
			*ret = strchr(strtok(Buf, "\n"), ' ');
			break;
		}
	}
	fclose(file);
	delete Buf;
	delete value;
}