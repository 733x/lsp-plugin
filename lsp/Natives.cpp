#define _CRT_SECURE_NO_WARNINGS

#include "SDK/amx/amx.h"
#include "SDK/plugincommon.h"

#include "Main.h"
#include "LSP.h"
#include "Natives.h"

LSP *g_LSP = new LSP;

cell AMX_NATIVE_CALL Natives::GetServerConfig(AMX *pAMX, cell *iParams)
{
	int size = iParams[3];
	cell *addr;
	char *dest = new char[size];
	char *value = new char[size];
	amx_GetAddr(pAMX, iParams[1], &addr);
	amx_GetString(dest, addr, 0, size);
	g_LSP->servercfgvalue(dest, &value, size * 4);
	amx_GetAddr(pAMX, iParams[2], &addr);
	amx_SetString(addr, value, 0, 0, size);
	logprintf("value %s", value);
	delete dest;
	delete value;
	return 1;
}