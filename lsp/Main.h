/* 
	Simple plugin for LSP
*/

#define VERSION		"0.1"
typedef void
	(*logprintf_t)(const char *, ...)
;

extern logprintf_t
	logprintf
;