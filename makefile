# Los Santos Playground
# LSP makefile

GPP = g++
GCC = gcc
LSP_FILE = "LSP.so"

COMPILE_FLAGS = -m32 -fPIC -c -O3 -w -D LINUX -D PROJECT_NAME=\"LSP\" -I ./amx/

LSP = -D LSP $(COMPILE_FLAGS)

all: LSP

clean: -rm -f *~ *.o *.so

LSP: clean
	$(GPP) $(LSP) *.cpp
	$(GPP) $(LSP) ./SDK/*.cpp
	$(GPP) -m32 -O2 -fshort-wchar -shared -o $(LSP_FILE) *.o